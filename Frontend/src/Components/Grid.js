import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import ToggleButtons from './ToggleButtons';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        direction: "column",
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        gridTemplateColumns: 500,
        gridGap: theme.spacing(3),
    },
    root: {
        minWidth: 275,
        height: 250,
        width: 500,
        marginLeft: 200,
        direction: "column",
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 14,
    },
}));

export default function GridGen() {
    const classes = useStyles();

    return (
        <div>
            <Grid container spacing={5} justify='center'>
                <Grid item xs={12}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                Placed 04/23/2020
                            </Typography>
                            <Typography variant="h5" component="h2">
                                OR12838
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Connected Email: lebronjames@gmail.com
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Number of Items: 5
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <ToggleButtons className={classes.toggleContainer}></ToggleButtons>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                Placed 04/21/2020
                            </Typography>
                            <Typography variant="h5" component="h2">
                                OR12345
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Connected Email: cg356266@wne.edu
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Number of Items: 7
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <ToggleButtons className={classes.toggleContainer}></ToggleButtons>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                Placed 04/10/2020
                            </Typography>
                            <Typography variant="h5" component="h2">
                                OR12112
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Connected Email: cdreyer@wne.edu
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Number of Items: 12
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <ToggleButtons className={classes.toggleContainer}></ToggleButtons>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                Placed 03/17/2020
                            </Typography>
                            <Typography variant="h5" component="h2">
                                OR12111
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Connected Email: sjackson@gmail.com
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Number of Items: 21
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <ToggleButtons className={classes.toggleContainer}></ToggleButtons>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                Placed 03/11/2020
                            </Typography>
                            <Typography variant="h5" component="h2">
                                OR11239
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Connected Email: as392034@wne.edu
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Number of Items: 6
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <ToggleButtons className={classes.toggleContainer}></ToggleButtons>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item xs={12}>
                    <Card className={classes.root}>
                        <CardContent>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                Placed 03/01/2020
                            </Typography>
                            <Typography variant="h5" component="h2">
                                OR12124
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Connected Email: disneylover@gmail.com
                            </Typography>
                            <Typography className={classes.pos} color="textSecondary">
                                Number of Items: 1000
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <ToggleButtons className={classes.toggleContainer}></ToggleButtons>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
        </div>
    );
}

/*
export default function SimpleCard() {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Word of the Day
          </Typography>
                <Typography variant="h5" component="h2">
                    be{bull}nev{bull}o{bull}lent
          </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    adjective
          </Typography>
                <Typography variant="body2" component="p">
                    well meaning and kindly.
            <br />
                    {'"a benevolent smile"'}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Learn More</Button>
            </CardActions>
        </Card>
    );
}

*/
/*import Grid from '@material-ui/core/Grid';
import React from 'react';
import approve_reject_buttons from './approve_reject_buttons';

export default class List extends React.Component {
    render() {
        return (
            <div>
                <Grid>
                    <div class="grid-container">
                        <div class="grid-item">OR12390: lebronjames@gmail.com</div>
                        <fieldset  approve_reject_buttons> <fieldset/>;
                        <div class="grid-item">OR12838: jm342193@wne.edu</div>
                        <approve_reject_buttons />;
                        <div class="grid-item">OR12432: cg356266@wne.edu</div>
                        <approve_reject_buttons />;
                        <div class="grid-item">OR16234: cdreyer@wne.edu</div>
                        <approve_reject_buttons />;
                        <div class="grid-item">OR12212: stoneyjackson@wne.edu</div>
                        <approve_reject_buttons />;
                    </div>
                </Grid>
            </div>
        );
    }
}*/