package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EntityScan(basePackages= {"com.example.demo.repository.usersRepository", "com.example.demo.controller.UsersController"})
@ComponentScan({"com.example.demo.repository.UsersRepository"})
@EnableMongoRepositories
@EnableAutoConfiguration
public class WnebnmMongoDbApplication{

	public static void main(String[] args) {
		SpringApplication.run(WnebnmMongoDbApplication.class, args);
	}

}
