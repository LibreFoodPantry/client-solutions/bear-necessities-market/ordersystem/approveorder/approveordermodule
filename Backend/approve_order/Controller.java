package api.approve_order.approveordermodule.Backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

@RestController
public class Controller {
    /**
     * Sends a list of orderIDs for all orders approved since a given time.
     * @param timestamp All sent orders should occur after this timestamp.
     * @return JSON Object [{"id", orderID}, ...]
     */
    @GetMapping("/orders/approved/time/{timestamp}")
    public JSONObject getOrdersSinceTime(@PathVariable long timestamp) {
        Map<String, String> orderMap = new HashMap<>();

        // Replace next line with orders from database
        Order order = new Order("OR1234", 4222020, "ab12345");
        orderMap.put("id", order.getOrderID());

        return new JSONObject(orderMap);
    }

    /**
     * Sends a GET request to place order for all recent orders.
     * @param timestamp The last time we asked for placed orders.
     */
    public ArrayList<Order> sendRequest(long timestamp) {
        ArrayList<Order> orders = new ArrayList<>();

        try {
            URL placeOrder = new URL ("api/order/time/" + timestamp);
            HttpURLConnection conn = (HttpURLConnection) placeOrder.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            orders = responseToArrayList(placeOrder);
            // Save orders to database

            conn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return orders;
    }

    /** Send Request helper function to parse the response into Orders and the database. */
    private ArrayList<Order> responseToArrayList(URL placeOrder) throws IOException {
        // Response to String
        String response = "";
        Scanner sc = new Scanner(placeOrder.openStream());
        while (sc.hasNext()) { response += sc.nextLine(); }
        sc.close();

        // String to ArrayList
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Order> orders = new ArrayList<>();

        String[] resultArray = response.split("},");
        for (String json : resultArray) {
            json = json.replace("\\{", "");
            json = json.replace("\\}", "");

            orders.add(mapper.readValue(json + "", Order.class));
        }
        return orders;
    }
}

