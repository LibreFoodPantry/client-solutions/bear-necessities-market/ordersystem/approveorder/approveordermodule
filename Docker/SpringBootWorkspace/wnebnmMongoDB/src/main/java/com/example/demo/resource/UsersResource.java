package com.example.demo.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Service;

import com.example.demo.document.Users;
import com.example.demo.repository.UsersRepository;

@Service
@EnableMongoRepositories
public class UsersResource {

	
	@Autowired
	private UsersRepository usersRepository;
	
	//Creates all items for repository
	public Users create(String orderID, ArrayList<String> items, int timeOrdered, String studentID, String status) {
		return usersRepository.save(new Users(orderID, timeOrdered, studentID, status));
	}
	
	//Retrieves all items for repository
	public List<Users> getAll(){
		return usersRepository.findAll();
	}
	
	public Users getByOrderID(String orderID) {
		return usersRepository.findByOrderID(orderID);
	}
	
	//Updates status of the order
	public Users update(String orderID, int timeOrdered, String studentID, String status){
		Users u = usersRepository.findByOrderID(orderID);
		u.setStatusWaiting();
		u.setTimeOrdered(timeOrdered);
		u.setStudentID(studentID);
		/*
		if (Users.getStatus() == "Approved") {
			usersRepository.update(status);
		}
		else if (Users.getStatus() == "Rejected") {
			usersRepository.update(status);
		}
	*/
	return usersRepository.save(u);	
	}
	
	public void delete(String orderID) {
		Users u = usersRepository.findByOrderID(orderID);
		usersRepository.delete(u);
	}

	public void deleteAll() {
		usersRepository.deleteAll();
	}
}
