package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.document.Users;
import com.example.demo.resource.UsersResource;

@RestController
public class UsersController {

	@Autowired
	private UsersResource usersResource;
	
	@RequestMapping("/create")
	public String create(@RequestParam String orderID, @RequestParam ArrayList<String> items, @RequestParam int timeOrdered, @RequestParam String studentID, @RequestParam String status) {
		Users u = usersResource.create(orderID, items, timeOrdered, studentID, status);
		return u.toString();
	}
	
	@RequestMapping("/get")
	public Users getUsers(@RequestParam String orderID) {
		return usersResource.getByOrderID(orderID);
	}
	
	@RequestMapping("/getAll")
	public List<Users> getAll(){
		return usersResource.getAll();
	}
	
	@RequestMapping("/update")
	public String update(String orderID, int timeOrdered, String studentID, String status) {
		Users u = usersResource.update(orderID, timeOrdered, studentID, status);
		return u.toString();
	}
	@RequestMapping("/deleteAll")
	public String deleteAll() {
		usersResource.deleteAll();
		return "Deleted all records";
	}
}
