import React, { Component } from 'react';
import Header from './Components/Header';
import GridList from '@material-ui/core/GridList'
import Box from '@material-ui/core/Box'
import logo from './logo.svg';
import './App.css';
import Grid from './Components/Grid';
import Footer from './Components/Footer';



export default class App extends Component {
  render() {
    return (
      <form>
        <Header />
        <Grid />
        <Footer />
        

      </form>
    );
  }
}
