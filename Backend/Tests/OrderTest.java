package api.approve_order.approveordermodule.Backend.Tests;

import api.approve_order.approveordermodule.Backend.Order;
import org.junit.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class OrderTest {
    Order order;

    @Before
    public void CreateOrder() {
        order = new Order("OR1234", 04222020, "ab12345");
    }

    @Test
    public void TestGetOrderID() {
        assertEquals(order.getOrderID(), "OR1234");
    }

    @Test
    public void TestSetStatus() {
        order.setStatusApproved();
        assertEquals(order.getStatus(), "Approved");
    }

    @Test
    public void TestUpdateStatus() {
        order.setStatusApproved();
        assertEquals(order.getStatus(), "Approved");
        order.setStatusRejected();
        assertEquals(order.getStatus(), "Rejected");
    }

    @Test
    public void TestAddItems() {
        assertNull(order.getItems());

        ArrayList<String> items = new ArrayList<>();
        items.add("Apples");
        items.add("Oranges");
        order.addItems(items);

        assertEquals(items, order.getItems());
    }
}
