package api.approve_order.approveordermodule.Backend.Tests;

import api.approve_order.approveordermodule.Backend.*;
import net.minidev.json.JSONObject;
import org.junit.Test;


import java.util.ArrayList;

public class ControllerTest {
    Controller controller = new Controller();
    long timestamp = 4272020;

    @Test
    public void TestGetOrdersSinceTime() {
        // Get Orders function still in progress...
        JSONObject result = controller.getOrdersSinceTime(timestamp);
    }

    @Test
    public void TestSendRequest() {
        // Test still in progress...
        ArrayList<Order> orders = controller.sendRequest(timestamp);
    }

}
