package com.example.demo.document;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "User")
public class Users {
	@Id
	String id;
	private String orderID;
	private ArrayList<String> items;
	private int timeOrdered; 
	private String studentID;
	static String status;
	
	//Constructor for all elements we need for an order.
	public Users(String orderID, int timeOrdered, String studentID, String status) {
		this.orderID = orderID;
		this.timeOrdered = timeOrdered;
		this.studentID = studentID;
		setStatusWaiting();
	}
	
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public void setItems(ArrayList<String> items) {
		this.items = items;
	}

	public void setTimeOrdered(int timeOrdered) {
		this.timeOrdered = timeOrdered;
	}

	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

	public static void setStatus(String status) {
		Users.status = status;
	}

	public String getOrderID() {
		return orderID;
	}
	
	public ArrayList<String> getItems() {
		return items;
	}
	
	public int getTimeOrdered() {
		return timeOrdered;
	}
	
	public String getStudentID() {
		return studentID;
	}
	
	public void setStatusWaiting() {
		status = "Waiting";
	}
	
	public void setStatusApproved() {
		status = "Approved";
	}
	
	public void setStatusRejected() {
		status = "Rejected";
	}
	
	public String getStatus() {
		return status;
	}
	
	public String toString() {
		return "Order ID:" + orderID + "Time Ordered:" + timeOrdered + "StudentID:" + studentID + "Status:" + status;
	}
}
