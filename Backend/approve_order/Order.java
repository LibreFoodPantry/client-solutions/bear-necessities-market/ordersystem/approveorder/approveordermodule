package api.approve_order;

import java.util.ArrayList;

public class Order {
    private final String orderID;
    private ArrayList<String> items;
    private final int timeOrdered;
    private final String studentID;
    private String status;

    /* Basic Constructor */
    public Order(String orderID, int timeOrdered, String studentID) {
        this.orderID = orderID;
        this.timeOrdered = timeOrdered;
        this.studentID = studentID;
        setStatusWaiting();
    }

    public String getOrderID() { return orderID; }

    /* Update the status of the order to Waiting, Approved, or Rejected. Add any database updates to these methods. */
    public void setStatusWaiting() { status = "Waiting"; }
    public void setStatusApproved() { status = "Approved"; }
    public void setStatusRejected() { status = "Rejected"; }

    /* Add food items to the order */
    public void addItems(ArrayList items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderID = " + orderID +
                ", timeOrdered = " + timeOrdered +
                ", studentID = " + studentID +
                ", status = " + status +
                '}';
    }
}
