
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SubmitButton from './SubmitButton';

const useStyles = makeStyles((theme) => ({
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    grow: {
        flexGrow: 1,
    },
}));

export default function BottomAppBar() {
    const classes = useStyles();

    return (
        <div>
            <AppBar edge="start" position="fixed" color="primary" position="static" className={classes.appBar}>
                <SubmitButton type='submit' className={classes.button}></SubmitButton>
            </AppBar>
        </div>
    );
}